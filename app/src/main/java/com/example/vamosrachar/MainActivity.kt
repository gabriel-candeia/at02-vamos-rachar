package com.example.vamosrachar

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import java.util.Locale

class MainActivity : AppCompatActivity(), TextWatcher, TextToSpeech.OnInitListener {
    private lateinit var tts: TextToSpeech

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val edtConta = findViewById<EditText>(R.id.edtConta)
        edtConta.addTextChangedListener(this)
        val edtPessoas = findViewById<EditText>(R.id.edtPessoas)
        edtPessoas.addTextChangedListener(this)

        tts = TextToSpeech(this,this)
    }

    fun clickFalar(v: View){
        val textView2 = findViewById<TextView>(R.id.textView2)
        tts.speak("Cada pessoa deve pagar "+textView2.text,TextToSpeech.QUEUE_FLUSH, null, null)
        Log.d("","falar")
    }

    fun clickCompartilhar(v: View){
        val valor = findViewById<TextView>(R.id.textView2).text
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, "Cada pessoa deve pagar $valor.")
            type = "text/plain"
        }
        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
        Log.d("","compartilhar")
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        //TODO("Not yet implemented")
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        //TODO("Not yet implemented")
    }

    override fun afterTextChanged(s: Editable?) {
        //TODO("Not yet implemented")
        val a = findViewById<EditText>(R.id.edtConta).text.toString().toDoubleOrNull()
        val b = findViewById<EditText>(R.id.edtPessoas).text.toString().toDoubleOrNull()

        if(a==null || b==null || b==0.0) {

        }
        else {
            val textView2 = findViewById<TextView>(R.id.textView2)
            textView2.text = "R$ "+ String.format("%.2f",a/b)
        }
    }

    override fun onInit(status: Int) {
        if(status == TextToSpeech.SUCCESS) {
            tts.language = Locale.getDefault()
        }
    }
}